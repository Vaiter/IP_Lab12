<?php
session_start();
require( "config.php" );
$result='';
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$login = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";
$user=User::getByLogin($login);
if ($action != 'registration' && $action != "logout" && $action != "login" && !$login ) {
  login();
  exit;
}
else if($login && $action!='logout'){
    $user=User::getByLogin($login);
    edit();
    exit;
}

switch ( $action ) {
    case 'registration':
        registration();
    break;
  case 'edit':
    edit();
    break;
  case 'logout':
    logout();
    break;
    case 'login':
        login();
        break;
  default:
    login();
}

function registration() {
    $result='';
    if( isset($_POST['save']) ){
        $user = new User($_POST);
        if($_POST['password']!=$_POST['passwordRepeat']){
            $result='Пароль не співпадає';
            require( TEMPLATE_PATH . "/registration.php" );
        }
        else if( User::getByLogin($user->login)){
            $result='Користувач з таким логіном вже існує';
            require( TEMPLATE_PATH . "/registration.php" );
        }
        else{
            $user->insert();
            $_SESSION['username']=$user->login;
            require( TEMPLATE_PATH . "/editUser.php" );
        }
    }
    else if(isset($_POST['cancel'])){
        require( TEMPLATE_PATH . "/editUser.php" );
    }
    else{
        require( TEMPLATE_PATH . "/registration.php" );
    }
}

function logout() {
  unset( $_SESSION['username'] );
    require( TEMPLATE_PATH . "/homepage.php" );
}

function login()
{
    $result='';
    if (isset($_POST['login'])) {
        $user=User::getByLogin($_POST['login']);
        if ($user->password == $_POST['password']) {
            $_SESSION['username'] = $user->login;
            require( TEMPLATE_PATH . "/editUser.php" );
        } else {
            $result = "Не вірний логін чи пароль";
            require(TEMPLATE_PATH . "/homepage.php");
        }
    } else {
        require(TEMPLATE_PATH . "/homepage.php");
    }
}

function edit(){
    $result='';
    if ( !isset($_SESSION['username']) ) {
        login();
        return;
    }
    if ( isset( $_POST['save'] ) ) {
        $user = User::getByLogin($_SESSION['username']);
        if($_POST['oldPassword']!=$user->password){
            $result= 'Пароль не співпадає';
            require(TEMPLATE_PATH . "/editUser.php");
        }
        else {
            $_POST['password']=$_POST['newPassword'];
            $user = new User($_POST);
            $user->update($_SESSION['username']);
            $_SESSION['username']=$user->login;
            require(TEMPLATE_PATH . "/editUser.php");
        }
  }
  elseif ( isset( $_POST['cancel'] ) ) {
      require(TEMPLATE_PATH . "/editUser.php");
  }
  else {
    require( TEMPLATE_PATH . "/editUser.php" );
  }
}
?>

