<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Europe/Kiev" );
define( "DB_DSN", "mysql:host=localhost;dbname=lab12" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "" );
define( "CLASS_PATH", "classes" );
define( "TEMPLATE_PATH", "templates" );
require( CLASS_PATH . "/User.php" );

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}
set_exception_handler( 'handleException' );
?>
